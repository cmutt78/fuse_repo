# See https://docs.chef.io/config_rb_knife.html for more information on knife configuration options

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "cmutt78"
client_key               "#{current_dir}/cmutt78.pem"
validation_client_name   "corpmutt-validator"
validation_key           "#{current_dir}/corpmutt-validator.pem"
chef_server_url          "https://api.opscode.com/organizations/corpmutt"
cookbook_path            ["#{current_dir}/../cookbooks"]
